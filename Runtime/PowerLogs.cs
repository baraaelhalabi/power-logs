namespace VentureStudios.PowerLogs {
    public static class PowerLogs {
        public static string ADLog(this string log) {
            return "(ADMOB): ".Color("aqua").Bold().Italic().Size(24) + log;
        }

        public static string FirebaseLog(this string log) {
            return "(FIREBASE): ".Color("yellow").Bold().Italic().Size(24) + log;
        }

        public static string UILog(this string log) {
            return "(UI): ".Color("teal").Bold().Italic().Size(24) + log;
        }

        public static string SoundLog(this string log) {
            return "(SOUND): ".Color("orange").Bold().Italic().Size(24) + log;
        }

        public static string Bold(this string log) {
            return $"<b>{log}</b>";
        }
        
        public static string Italic(this string log) {
            return $"<i>{log}</i>";
        }
        
        public static string Color(this string log, string color) {
            return $"<color={color}>{log}</color>";
        }

        public static string Size(this string log, int size) {
            return $"<size={size}>{log}</size>";
        }
    }
}
