using NUnit.Framework;
using VentureStudios.PowerLogs;

public class Tests {
    [Test]
    public void TestAdLog() {
        const string log = "t";
        var test = log.ADLog();
        Assert.AreEqual("<size=24><i><b><color=aqua>(ADMOB): </color></b></i></size>t", test);
    }
    
    [Test]
    public void TestFirebaseLog() {
        const string log = "t";
        var test = log.FirebaseLog();
        Assert.AreEqual("<size=24><i><b><color=yellow>(FIREBASE): </color></b></i></size>t", test);
    }
    
    [Test]
    public void TestUILog() {
        const string log = "t";
        var test = log.UILog();
        Assert.AreEqual("<size=24><i><b><color=teal>(UI): </color></b></i></size>t", test);
    }

    [Test]
    public void TestSoundLog() {
        const string log = "t";
        var test = log.SoundLog();
        Assert.AreEqual("<size=24><i><b><color=orange>(SOUND): </color></b></i></size>t", test);
    }

    [Test]
    public void TestBold() {
        const string log = "t";
        var test = log.Bold();
        Assert.AreEqual("<b>t</b>", test);
    }
    
    [Test]
    public void TestItalic() {
        const string log = "t";
        var test = log.Italic();
        Assert.AreEqual("<i>t</i>", test);
    }
    
    [Test]
    public void TestColor() {
        const string log = "t";
        var test = log.Color("red");
        Assert.AreEqual("<color=red>t</color>", test);
    }
    
    [Test]
    public void TestSize() {
        const string log = "t";
        var test = log.Size(12);
        Assert.AreEqual("<size=12>t</size>", test);
    }
}
